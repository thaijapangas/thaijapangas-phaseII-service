﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using System.Globalization;
using iWebServices.Functions;
using System.Data;
using iEntitiesTmp.DistributionRet;
using iWebServices.Objects;
using System.Configuration;
using iEntitiesTmp.Customers;

namespace iWebServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class Service : IService
    {
        #region Start

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }
        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        #endregion

        #region Test ConnectionDB
        /*
        public string CheckExpireSoftware()
        {
            string connti = "FALSE";
            DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;
            using (dbModels.TireStockEntities db = new dbModels.TireStockEntities())
            {
                string _expire_date = db.t_setup.FirstOrDefault().expire_date;
                if (string.IsNullOrEmpty(_expire_date))
                {
                    connti = "FALSE";
                }
                else if (!string.IsNullOrEmpty(_expire_date))
                {
                    string _decry_expire_date = Securitys.Decrystion.DecryMD5.Decrypt(_expire_date, true);
                    if (_decry_expire_date.Trim() == "ActivateKeys")
                    {
                        connti = "LIFETIME";
                    }
                    else
                    {
                        // string _decry_expire_date = Securitys.Decrystion.DecryMD5.Decrypt(_expire_date, true);
                        string _crrent_date = Convert.ToDateTime(GetDateServer()).ToString("yyyyMMdd", usDtfi);

                        if (Convert.ToInt32(_decry_expire_date) < Convert.ToInt32(_crrent_date))
                            connti = "FALSE";
                        else if (Convert.ToInt32(_decry_expire_date) > Convert.ToInt32(_crrent_date))
                            connti = "TRUE";
                        else if (string.IsNullOrEmpty(_decry_expire_date))
                            connti = "TRUE";
                    }
                }
            }

            return connti;
        }
        */
        public bool ConnectDB()
        {
            Boolean Result = false;
            SqlConnection resultConn = (SqlConnection)SqlConn();
            if (string.IsNullOrEmpty(resultConn.ConnectionString))
            {
                Result = false;
            }
            else
            {
                #region SUCCESS

                // SqlConnection conn = (SqlConnection)SqlConn();
                // SqlConnection conn = (SqlConnection)resultConn;

                try
                {
                    // conn.Open();
                    resultConn.Open();
                    Result = true;
                }
                catch (Exception)
                {
                    Result = false;
                }
                finally
                {
                    resultConn.Dispose();
                    // conn.Dispose();
                }

                #endregion
            }
            return Result;
        }
        public object SqlConn()
        {
            try
            {
                string strConn = System.Configuration.ConfigurationManager.ConnectionStrings["TJGDBEntities"].ConnectionString;
                int StartIndex = strConn.IndexOf("data source");
                int StartIndexs = strConn.IndexOf("multipleactiveresultsets");
                string sub = strConn.Substring(StartIndex, StartIndexs - StartIndex);
                SqlConnection sql = new SqlConnection(sub);
                return sql;
            }
            catch (Exception)
            {
                SqlConnection sql = new SqlConnection();
                return sql;
            }
        }
        /*
        public string ValidationLicence(string _licence_keys)
        {
            string Result = string.Empty;
            try
            {
                DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;
                string _licence_key = Securitys.Decrystion.DecryMD5.Decrypt(_licence_keys, true);
                string _crrent_date = Convert.ToDateTime(GetDateServer()).ToString("yyyyMMdd", usDtfi);
                if (!string.IsNullOrEmpty(_licence_key))
                {
                    if (_licence_key.Trim() == "ActivateKeys")
                    {
                        if (ActivateLicence(_licence_keys))
                        {
                            Result = "TRUE";
                        }
                    }
                    else if (_licence_key.IndexOf("2016") >= 0)
                    {
                        if (ActivateLicence(_licence_keys))
                        {
                            DateTime dTime = DateTime.ParseExact(_licence_key, "yyyyMMdd", null);
                            string display_date = dTime.ToLongDateString();
                            Result = "TRUE_EXPRIE|" + display_date;
                        }
                    }
                    else if (Convert.ToInt32(_licence_key) < Convert.ToInt32(_crrent_date))
                    {
                        Result = "TRUE_EXPIRED";
                    }
                    else
                    {
                        Result = "FALSE";
                    }
                }
            }
            catch (Exception)
            {
                Result = "FALSE";
            }

            return Result;
        }
        */

        #endregion

        #region GetDate

        public object GetDateServer()
        {
            using (dbModels.TJGDBEntities db = new dbModels.TJGDBEntities())
            {
                return db.sp_GetDateServer().First().Value;
            }
        }

        #endregion

        #region IsOnline

        public string IsOnline()
        {
            try
            {
                using (dbModels.TJGDBEntities db = new dbModels.TJGDBEntities())
                {
                    return db.isOnline().First();
                }
            }
            catch
            {
                return "OFFLINE";
            }
        }

        #endregion

        #region Distribution Ret

        public string InsUpdDistributionRet(byte[] Data)
        {
            string retuenResult = string.Empty;
            System.Data.Objects.ObjectParameter _Errcode = new System.Data.Objects.ObjectParameter("out_vchCode", typeof(string));
            System.Data.Objects.ObjectParameter _ErrMsg = new System.Data.Objects.ObjectParameter("out_vchMessage", typeof(string));
            try
            {
                var data = Data.ConvertToDataSet();
                DataTable dtDisRet = data.Tables["Distribution_Ret"];

                using (dbModels.TJGDBEntities db = new dbModels.TJGDBEntities())
                {
                    foreach (DataRow item in dtDisRet.Rows)
                    {
                        DateTime CreateDate = (DateTime)item["create_date"];

                        db.sp_insupt_t_distribution_ret(item["serial_number"].ToString(),
                                                        item["cust_id"].ToString(),
                                                        item["doc_no"].ToString(),
                                                        item["vehicle"].ToString(),
                                                        item["empty_or_full"].ToString(),
                                                        item["create_date"].ToString(),
                                                        _Errcode,
                                                        _ErrMsg);

                        if (_Errcode.Value.ToString().Trim() == "0")
                            retuenResult = "0";
                        else if (_Errcode.Value.ToString().Trim() == "ER0098")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                        else if (_Errcode.Value.ToString().Trim() == "ER0099")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                    }
                }
            }
            catch (Exception ex)
            {
                if (_Errcode.Value.ToString().Trim() == "ER0099")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else if (_Errcode.Value.ToString().Trim() == "ER0098")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else
                    retuenResult = ex.Message.ToString();
            }

            return retuenResult;
        }
        public string DoInsUpdDistributionRet(string serial_number, string cust_id, string doc_no, string vehicle, string empty_or_full, string create_date)
        {
            string retuenResult = string.Empty;
            System.Data.Objects.ObjectParameter _Errcode = new System.Data.Objects.ObjectParameter("out_vchCode", typeof(string));
            System.Data.Objects.ObjectParameter _ErrMsg = new System.Data.Objects.ObjectParameter("out_vchMessage", typeof(string));
            try
            {
                /*
                var data = Data.ConvertToDataSet();
                DataTable dtDisRet = data.Tables["Distribution_Ret"];
                */

                using (dbModels.TJGDBEntities db = new dbModels.TJGDBEntities())
                {
                    DateTime dtTime = DateTime.ParseExact(create_date, "M/d/yyyy h:mm:ss tt", new CultureInfo("en-US"), DateTimeStyles.None);
                    string CreateDate = dtTime.ToString("yyyy-MM-dd HH:mm:ss");
                    db.sp_insupt_t_distribution_ret(serial_number,
                                                    cust_id,
                                                    doc_no,
                                                    vehicle,
                                                    empty_or_full,
                                                    CreateDate,
                                                    _Errcode,
                                                    _ErrMsg);

                    if (_Errcode.Value.ToString().Trim() == "0")
                        retuenResult = "0";
                    else if (_Errcode.Value.ToString().Trim() == "ER0098")
                        retuenResult = _ErrMsg.Value.ToString().Trim();
                    else if (_Errcode.Value.ToString().Trim() == "ER0099")
                        retuenResult = _ErrMsg.Value.ToString().Trim();
                }
            }
            catch (Exception ex)
            {
                if (_Errcode.Value.ToString().Trim() == "ER0099")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else if (_Errcode.Value.ToString().Trim() == "ER0098")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else
                    retuenResult = ex.Message.ToString();
            }

            return retuenResult;
        }

        #endregion

        #region GI Delivery

        public string InsUpdGIDelivery(byte[] Data)
        {
            string retuenResult = string.Empty;
            System.Data.Objects.ObjectParameter _Errcode = new System.Data.Objects.ObjectParameter("out_vchCode", typeof(string));
            System.Data.Objects.ObjectParameter _ErrMsg = new System.Data.Objects.ObjectParameter("out_vchMessage", typeof(string));
            try
            {
                var data = Data.DeCompressByDotNetZip().ConvertToDataSet();
                DataTable dtGIDelivery = data.Tables["GI_Delivery"];

                using (dbModels.TJGDBEntities db = new dbModels.TJGDBEntities())
                {
                    foreach (DataRow item in dtGIDelivery.Rows)
                    {
                        db.sp_insupt_t_gi_delivery(item["serial_number"].ToString(),
                                                   item["doc_no"].ToString(),
                                                   item["create_date"].ToString(),
                                                   _Errcode,
                                                   _ErrMsg);

                        if (_Errcode.Value.ToString().Trim() == "0")
                            retuenResult = "0";
                        else if (_Errcode.Value.ToString().Trim() == "ER0098")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                        else if (_Errcode.Value.ToString().Trim() == "ER0099")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                    }
                }
            }
            catch (Exception ex)
            {
                if (_Errcode.Value.ToString().Trim() == "ER0099")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else if (_Errcode.Value.ToString().Trim() == "ER0098")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else
                    retuenResult = ex.Message.ToString();
            }

            return retuenResult;
        }
        public string DoInsUpdGIDelivery(string serial_number, string do_no, string create_date)
        {
            string retuenResult = string.Empty;
            System.Data.Objects.ObjectParameter _Errcode = new System.Data.Objects.ObjectParameter("out_vchCode", typeof(string));
            System.Data.Objects.ObjectParameter _ErrMsg = new System.Data.Objects.ObjectParameter("out_vchMessage", typeof(string));
            try
            {
                /*
                var data = Data.ConvertToDataSet();
                DataTable dtDisRet = data.Tables["Distribution_Ret"];
                */

                using (dbModels.TJGDBEntities db = new dbModels.TJGDBEntities())
                {
                    DateTime dtTime = DateTime.ParseExact(create_date, "M/d/yyyy h:mm:ss tt", new CultureInfo("en-US"), DateTimeStyles.None);
                    string CreateDate = dtTime.ToString("yyyy-MM-dd HH:mm:ss");
                    db.sp_insupt_t_gi_delivery(serial_number,
                                               do_no,
                                               CreateDate,
                                               _Errcode,
                                               _ErrMsg);

                    if (_Errcode.Value.ToString().Trim() == "0")
                        retuenResult = "0";
                    else if (_Errcode.Value.ToString().Trim() == "ER0098")
                        retuenResult = _ErrMsg.Value.ToString().Trim();
                    else if (_Errcode.Value.ToString().Trim() == "ER0099")
                        retuenResult = _ErrMsg.Value.ToString().Trim();
                }
            }
            catch (Exception ex)
            {
                if (_Errcode.Value.ToString().Trim() == "ER0099")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else if (_Errcode.Value.ToString().Trim() == "ER0098")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else
                    retuenResult = ex.Message.ToString();
            }

            return retuenResult;
        }

        #endregion

        #region GI Ref IO Empty

        public string InsUpdGIRefIOEmpty(byte[] Data)
        {
            string retuenResult = string.Empty;
            System.Data.Objects.ObjectParameter _Errcode = new System.Data.Objects.ObjectParameter("out_vchCode", typeof(string));
            System.Data.Objects.ObjectParameter _ErrMsg = new System.Data.Objects.ObjectParameter("out_vchMessage", typeof(string));
            try
            {
                var data = Data.DeCompressByDotNetZip().ConvertToDataSet();
                DataTable dtDIOEmpt = data.Tables["GI_Ref_IO_Empty"];

                using (dbModels.TJGDBEntities db = new dbModels.TJGDBEntities())
                {
                    foreach (DataRow item in dtDIOEmpt.Rows)
                    {
                        db.sp_insupt_t_gi_ref_io_empty(item["serial_number"].ToString(),
                                                       item["doc_no"].ToString(),
                                                       item["create_date"].ToString(),
                                                       _Errcode,
                                                       _ErrMsg);

                        if (_Errcode.Value.ToString().Trim() == "0")
                            retuenResult = "0";
                        else if (_Errcode.Value.ToString().Trim() == "ER0098")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                        else if (_Errcode.Value.ToString().Trim() == "ER0099")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                    }
                }
            }
            catch (Exception ex)
            {
                if (_Errcode.Value.ToString().Trim() == "ER0099")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else if (_Errcode.Value.ToString().Trim() == "ER0098")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else
                    retuenResult = ex.Message.ToString();
            }

            return retuenResult;
        }
        public string DoInsGIRefIOEmpty(string serial_number, string do_no, string create_date)
        {
            string retuenResult = string.Empty;
            System.Data.Objects.ObjectParameter _Errcode = new System.Data.Objects.ObjectParameter("out_vchCode", typeof(string));
            System.Data.Objects.ObjectParameter _ErrMsg = new System.Data.Objects.ObjectParameter("out_vchMessage", typeof(string));
            try
            {
                /*
                var data = Data.ConvertToDataSet();
                DataTable dtDisRet = data.Tables["Distribution_Ret"];
                */

                using (dbModels.TJGDBEntities db = new dbModels.TJGDBEntities())
                {
                    DateTime dtTime = DateTime.ParseExact(create_date, "M/d/yyyy h:mm:ss tt", new CultureInfo("en-US"), DateTimeStyles.None);
                    string CreateDate = dtTime.ToString("yyyy-MM-dd HH:mm:ss");
                    db.sp_insupt_t_gi_ref_io_empty(serial_number,
                                                   do_no,
                                                   CreateDate,
                                                   _Errcode,
                                                   _ErrMsg);

                    if (_Errcode.Value.ToString().Trim() == "0")
                        retuenResult = "0";
                    else if (_Errcode.Value.ToString().Trim() == "ER0098")
                        retuenResult = _ErrMsg.Value.ToString().Trim();
                    else if (_Errcode.Value.ToString().Trim() == "ER0099")
                        retuenResult = _ErrMsg.Value.ToString().Trim();
                }
            }
            catch (Exception ex)
            {
                if (_Errcode.Value.ToString().Trim() == "ER0099")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else if (_Errcode.Value.ToString().Trim() == "ER0098")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else
                    retuenResult = ex.Message.ToString();
            }

            return retuenResult;
        }

        #endregion

        #region GR Production

        public string InsUpdGRProduction(byte[] Data)
        {
            string retuenResult = string.Empty;
            System.Data.Objects.ObjectParameter _Errcode = new System.Data.Objects.ObjectParameter("out_vchCode", typeof(string));
            System.Data.Objects.ObjectParameter _ErrMsg = new System.Data.Objects.ObjectParameter("out_vchMessage", typeof(string));
            try
            {
                var data = Data.DeCompressByDotNetZip().ConvertToDataSet();
                DataTable dtGRProduction = data.Tables["GR_Production"];

                using (dbModels.TJGDBEntities db = new dbModels.TJGDBEntities())
                {
                    foreach (DataRow item in dtGRProduction.Rows)
                    {
                        db.sp_insupt_t_gr_production(item["serial_number"].ToString(),
                                                     item["pre_order"].ToString(),
                                                     item["batch"].ToString(),
                                                     item["create_date"].ToString(),
                                                     _Errcode,
                                                     _ErrMsg);

                        if (_Errcode.Value.ToString().Trim() == "0")
                            retuenResult = "0";
                        else if (_Errcode.Value.ToString().Trim() == "ER0098")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                        else if (_Errcode.Value.ToString().Trim() == "ER0099")
                            retuenResult = _ErrMsg.Value.ToString().Trim();
                    }
                }
            }
            catch (Exception ex)
            {
                if (_Errcode.Value.ToString().Trim() == "ER0099")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else if (_Errcode.Value.ToString().Trim() == "ER0098")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else
                    retuenResult = ex.Message.ToString();
            }

            return retuenResult;
        }
        public string DoInsGRProduction(string serial_number, string pre_order, string batch, string create_date)
        {
            string retuenResult = string.Empty;
            System.Data.Objects.ObjectParameter _Errcode = new System.Data.Objects.ObjectParameter("out_vchCode", typeof(string));
            System.Data.Objects.ObjectParameter _ErrMsg = new System.Data.Objects.ObjectParameter("out_vchMessage", typeof(string));
            try
            {
                /*
                var data = Data.ConvertToDataSet();
                DataTable dtDisRet = data.Tables["Distribution_Ret"];
                */

                using (dbModels.TJGDBEntities db = new dbModels.TJGDBEntities())
                {
                    DateTime dtTime = DateTime.ParseExact(create_date, "M/d/yyyy h:mm:ss tt", new CultureInfo("en-US"), DateTimeStyles.None);
                    string CreateDate = dtTime.ToString("yyyy-MM-dd HH:mm:ss");
                    db.sp_insupt_t_gr_production(serial_number,
                                                 pre_order,
                                                 batch,
                                                 CreateDate,
                                                 _Errcode,
                                                 _ErrMsg);

                    if (_Errcode.Value.ToString().Trim() == "0")
                        retuenResult = "0";
                    else if (_Errcode.Value.ToString().Trim() == "ER0098")
                        retuenResult = _ErrMsg.Value.ToString().Trim();
                    else if (_Errcode.Value.ToString().Trim() == "ER0099")
                        retuenResult = _ErrMsg.Value.ToString().Trim();
                }
            }
            catch (Exception ex)
            {
                if (_Errcode.Value.ToString().Trim() == "ER0099")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else if (_Errcode.Value.ToString().Trim() == "ER0098")
                    retuenResult = _ErrMsg.Value.ToString().Trim();
                else
                    retuenResult = ex.Message.ToString();
            }

            return retuenResult;
        }

        #endregion

        #region Download Customer

        public DataTable DoLoadCustomer()
        {
            DataTable dtResult = new DataTable();
            dtResult.TableName = "Customer";
            string _loadCustomer = ConfigurationManager.AppSettings["LoadCustomer"].ToString().Trim();
            if (!string.IsNullOrEmpty(_loadCustomer))
            {
                if (_loadCustomer.ToUpper().Trim() == "YES")
                {
                    #region Config Get by is active

                    using (dbModels.TJGDBEntities db = new dbModels.TJGDBEntities())
                    {
                        var result = ObjectConverter.ToDataTable(from rows in db.tbm_customer
                                                                 where rows.is_active == "YES"
                                                                 select new entCustomer
                                                                 {
                                                                     #region Filds

                                                                     customer_code = rows.customer_code,
                                                                     customer_name = rows.customer_name

                                                                     #endregion
                                                                 });
                        dtResult = result;
                    }

                    #endregion
                }
                else
                {
                    #region Config Get ALL

                    using (dbModels.TJGDBEntities db = new dbModels.TJGDBEntities())
                    {
                        var result = ObjectConverter.ToDataTable(from rows in db.tbm_customer
                                                                 select new entCustomer
                                                                 {
                                                                     #region Filds

                                                                     customer_code = rows.customer_code,
                                                                     customer_name = rows.customer_name

                                                                     #endregion
                                                                 });

                        dtResult = result;
                    }

                    #endregion
                }
            }

            return dtResult;
        }
        public ServiceResponse DoLoadCustomerByte()
        {
            ServiceResponse response = new ServiceResponse();
            string _loadCustomer = ConfigurationManager.AppSettings["LoadCustomer"].ToString().Trim();
            if (!string.IsNullOrEmpty(_loadCustomer))
            {
                if (_loadCustomer.ToUpper().Trim() == "YES")
                {
                    #region Config Get by is active

                    using (dbModels.TJGDBEntities db = new dbModels.TJGDBEntities())
                    {
                        var result = ObjectConverter.ToDataTable(from rows in db.tbm_customer
                                                                 where rows.is_active == "YES"
                                                                 select new entCustomer
                                                                 {
                                                                     #region Filds

                                                                     customer_code = rows.customer_code,
                                                                     customer_name = rows.customer_name

                                                                     #endregion
                                                                 });
                        DataSet ds = new DataSet("Customer");
                        ds.Tables.Add(result);
                        response.Data = ds.ConvertToByteArray().CompressByDotNetZip();
                        
                    }

                    #endregion
                }
                else
                {
                    #region Config Get ALL

                    using (dbModels.TJGDBEntities db = new dbModels.TJGDBEntities())
                    {
                        var result = ObjectConverter.ToDataTable(from rows in db.tbm_customer
                                                                 select new entCustomer
                                                                 {
                                                                     #region Filds

                                                                     customer_code = rows.customer_code,
                                                                     customer_name = rows.customer_name

                                                                     #endregion
                                                                 });

                        DataSet ds = new DataSet("Customer");
                        ds.Tables.Add(result);
                        response.Data = ds.ConvertToByteArray().CompressByDotNetZip();
                    }

                    #endregion
                }
            }

            return response;
        }
        public DataTable DoLoadCustomerStored()
        {
            DataTable dtResult = new DataTable();
            try
            {
                System.Data.Objects.ObjectParameter _Errcode = new System.Data.Objects.ObjectParameter("out_vchCode", typeof(string));
                System.Data.Objects.ObjectParameter _ErrMsg = new System.Data.Objects.ObjectParameter("out_vchMessage", typeof(string));
                string _loadCustomer = ConfigurationManager.AppSettings["LoadCustomer"].ToString().Trim();
                if (!string.IsNullOrEmpty(_loadCustomer))
                {
                    using (dbModels.TJGDBEntities db = new dbModels.TJGDBEntities())
                    {
                        var result = db.sp_import_customer("YES", _Errcode, _ErrMsg);

                        dtResult = ObjectConverter.ToTable(result);
                    }
                }
                else
                {
                    using (dbModels.TJGDBEntities db = new dbModels.TJGDBEntities())
                    {
                        var result = db.sp_import_customer(string.Empty, _Errcode, _ErrMsg);

                        dtResult = ObjectConverter.ToTable(result);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return dtResult;
        }

        #endregion
    }
}