﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.Collections.Generic;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for ObjectConverter
/// </summary>
public static class ObjectConverter
{

    public static DataTable ToDataTable<T>(IEnumerable<T> MyObject)
    {
        DataTable table = new DataTable("dtNew");
        DataColumnCollection dataColumns = table.Columns;
        try { PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
            for (int i = 0; i < props.Count; i++) { PropertyDescriptor prop = props[i];
                dataColumns.Add(new DataColumn(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType)); }
            object[] values = new object[props.Count];
            foreach (T item in MyObject) {
                for (int i = 0; i < values.Length; i++) { values[i] = props[i].GetValue(item) ?? DBNull.Value; }
                table.Rows.Add(values); } }
        catch { throw; }
        return table;
    }
	public static bool Exclude(this System.Reflection.PropertyInfo _propertyInfo)
	{
		if ((_propertyInfo.PropertyType.BaseType == typeof(System.Data.Objects.DataClasses.RelatedEnd)) ||
					(_propertyInfo.PropertyType.BaseType == typeof(System.Data.Objects.DataClasses.EntityObject)) ||
					(_propertyInfo.PropertyType.BaseType == typeof(System.Data.Objects.DataClasses.EntityReference)) ||
					(_propertyInfo.PropertyType == typeof(System.Data.EntityState)) ||
					(_propertyInfo.PropertyType == typeof(System.Data.EntityKey)))
			return true;
		return false;
	}
	public static bool Exclude(this System.ComponentModel.PropertyDescriptor _propertyDescriptor)
	{
		if ((_propertyDescriptor.PropertyType.BaseType == typeof(System.Data.Objects.DataClasses.RelatedEnd)) ||
				 (_propertyDescriptor.PropertyType.BaseType == typeof(System.Data.Objects.DataClasses.EntityObject)) ||
				 (_propertyDescriptor.PropertyType.BaseType == typeof(System.Data.Objects.DataClasses.EntityReference)) ||
				 (_propertyDescriptor.PropertyType == typeof(System.Data.EntityState)) ||
				 (_propertyDescriptor.PropertyType == typeof(System.Data.EntityKey)))
			return true;
		return false;
	}
	public static global::System.Data.DataTable ToTable<T>(this IEnumerable<T> _list)
	{
		var dataTable = new System.Data.DataTable();
		dataTable.TableName = "dtNew";
		var colmn = new List<string>();
		try
		{
			var props = System.ComponentModel.TypeDescriptor.GetProperties(typeof(T));
			for (int i = 0; i < props.Count; i++)
			{
				var _prop = props[i];
				if (Exclude(_prop)) continue;
				dataTable.Columns.Add(_prop.Name, Nullable.GetUnderlyingType(_prop.PropertyType) ?? _prop.PropertyType);
				colmn.Add(_prop.Name);
			}
			System.Data.DataRow dataRow;
			foreach (T item in _list)
			{
				dataRow = dataTable.NewRow();
				foreach (string str in colmn)
				{
					dataRow[str] = props[str].GetValue(item) ?? DBNull.Value;
				}
				dataTable.Rows.Add(dataRow);
			}
		}
		catch { throw; }
		return dataTable;
	}
}