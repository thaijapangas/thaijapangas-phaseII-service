﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Data;
using System.Globalization;
using System.Data.Common;
using System.Xml.Linq;
using System.Xml;
using System.Reflection;

namespace iWebServices.Functions
{
    public static class StreamExtensions
    {

        public static byte[] ToBytes(this Stream instance, long dataLength)
        {
            if (instance == null)
                return null;

            byte[] gzipCompressedBytes = null;

            int bufferSize = 4096;
            long index = 0;

            byte[] bufferBytes = new byte[bufferSize];

            using (MemoryStream ms = new MemoryStream())
            {
            
                BinaryReader reader = new BinaryReader(instance);

                while (index < dataLength)
                {
                    bufferBytes = reader.ReadBytes(bufferSize);

                    ms.Write(bufferBytes, 0, bufferBytes.Length);

                    index += bufferSize;
                }

                reader.Close();                    
                

                gzipCompressedBytes = ms.ToArray();

                ms.Close();
            }


            instance.Close();
            instance.Dispose();


            return gzipCompressedBytes;
        }
    }

    public static class ObjectExtensions
    {

        public static byte[] Serialize(this object instance)
        {
            byte[] serializedBytes = null;

            using (MemoryStream ms = new MemoryStream())
            {
                
                if (instance == null)
                    return null;

                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, instance);
                
                serializedBytes = ms.ToArray();

                ms.Close();
            }

            return serializedBytes;
        }
        
        public static T DeSerialize<T>(this byte[] serializedBytes)
        {
            T obj = default(T);

            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter binForm = new BinaryFormatter();
                ms.Write(serializedBytes, 0, serializedBytes.Length);
                ms.Seek(0, SeekOrigin.Begin);
                obj = (T)binForm.Deserialize(ms);

                ms.Close();
            }

            return obj;
        }
    }

    public static class ADONetClassesExtensions
    {
        public static T GetValue<T>(this DbDataReader instance, string fieldName)
        {
            object o = instance[fieldName];
            if (o == DBNull.Value || o == null)
                return default(T);
            else
                return (T)o;
        }

        public static T GetValue<T>(this DbDataReader instance, string fieldName, T valueIfDbNull)
        {
            object o = instance[fieldName];
            if (o == DBNull.Value || o == null)
                return valueIfDbNull;
            else
                return (T)o;
        }

        public static T GetValue<T>(this DbDataReader instance, string fieldName, T valueIfDbNull, Func<object, T> converter)
        {
            object o = instance[fieldName];
            if (o == DBNull.Value || o == null)
                return valueIfDbNull;
            else
                return converter(o);
        }

        public static TReturn GetValue<TData, TReturn>(this DbDataReader instance, string columnName, TData valueIfDbNull, Func<TData, TReturn> converter)
        {
            object o = instance[columnName];
            TData data = default(TData);

            if (o == DBNull.Value || o == null)
                data = valueIfDbNull;
            else
                data = (TData)o;

            return converter(data);
        }

        public static IEnumerable<T> GetValues<T>(this DbDataReader instance, string[] columnNames)
        {
            foreach (string c in columnNames)
            {
                object o = instance[c];
                if (o != DBNull.Value && o != null)
                    yield return (T)o;
            }
        }

        public static IEnumerable<T> GetValues<T>(this DbDataReader instance, string[] columnNames, Func<T, bool> cond)
        {
            foreach (string c in columnNames)
            {
                object o = instance[c];
                if (o != DBNull.Value 
                    && o != null
                    && cond((T)o))
                {
                    yield return (T)o;
                }
            }
        }

        public static T GetValue<T>(this DataRow instance, string columnName)
        {
            object o = instance[columnName];
            if (o == DBNull.Value || o == null)
                return default(T);
            else
                return (T)o;
        }

        public static T GetValue<T>(this DataRow instance, string columnName, T valueIfDbNull)
        {
            object o = instance[columnName];
            if (o == DBNull.Value || o == null)
                return valueIfDbNull;
            else
                return (T)o;
        }

        public static TReturn GetValue<TData, TReturn>(this DataRow instance, string columnName, TData valueIfDbNull, Func<TData, TReturn> converter)
        {
            object o = instance[columnName];
            TData data = default(TData);

            if (o == DBNull.Value || o == null)
                data = valueIfDbNull;
            else
                data = (TData)o;

            return converter(data);
        }

        public static T GetValue<T>(this DataRow instance, string columnName, T valueIfDbNull, Func<object, T> converter)
        {
            object o = instance[columnName];
            if (o == DBNull.Value || o == null)
                return valueIfDbNull;
            else
                return converter(o);
        }

        public static IEnumerable<T> GetValues<T>(this DataRow instance, string[] columnNames)
        {
            foreach (string c in columnNames)
            {
                object o = instance[c];
                if (o != DBNull.Value)
                    yield return (T)o;
            }
        }

        public static IEnumerable<T> GetValues<T>(this DataRow instance, string[] columnNames, Func<T, bool> cond)
        {
            foreach (string c in columnNames)
            {
                object o = instance[c];
                if (o != DBNull.Value
                    && o != null 
                    && cond((T)o))
                {
                    yield return (T)o;
                }
            }
        }
    }

    public static class ArrayAndListExtensions
    {

        public static TValue GetValueByKey<TKey, TValue>(this KeyValuePair<TKey, TValue>[] instance, TKey key) where TKey : IComparable<TKey>
        {
            var result = (from item in instance
                          where item.Key.Equals(key)
                          select item.Value).FirstOrDefault();

            return result;
        }

        public static void SetItem<TKey, TValue>(this KeyValuePair<TKey, TValue>[] instance, int index, TKey key, TValue value)
        {
            instance[index] = new KeyValuePair<TKey, TValue>(key, value);
        }


        public static TValue GetValueByKey<TKey, TValue>(this List<KeyValuePair<TKey, TValue>> instance, TKey key) where TKey : IComparable<TKey>
        {
            var result = (from item in instance
                          where item.Key.Equals(key)
                          select item.Value).FirstOrDefault();

            return result;
        }

        public static void SetItem<TKey, TValue>(this List<KeyValuePair<TKey, TValue>> instance, TKey key, TValue value)
        {
            instance.Add(new KeyValuePair<TKey, TValue>(key, value));
        }

    }

    public static class DataSetExtensions
    {
        public static bool HasData(this DataSet instance)
        {
            if (instance == null)
                return false;

            foreach (DataTable table in instance.Tables)
            {
                if (table.Rows.Count > 0)
                    return true;
            }

            return false;
        }

        public static byte[] ConvertToByteArray(this DataSet instance)
        {
            if (instance == null)
                return null;

            byte[] dataBytes = null;
            long formatFlag = 0;    //no binary data
            long datasetPartLength = 0;

            using (MemoryStream ms = new MemoryStream())
            {
                instance.WriteXml(ms, XmlWriteMode.WriteSchema);

                byte[] datasetBytes = ms.ToArray();
                datasetPartLength = datasetBytes.Length;

                dataBytes = new byte[8 + datasetPartLength];

                Buffer.BlockCopy(BitConverter.GetBytes(formatFlag), 0, dataBytes, 0, 8);
                Buffer.BlockCopy(datasetBytes, 0, dataBytes, 8, (int)datasetPartLength);
            }

            //using (MemoryStream ms = new MemoryStream())
            //{
            //    using (XmlTextWriter w = new XmlTextWriter(ms, Encoding.UTF8))
            //    {
            //        instance.WriteXml(w, XmlWriteMode.WriteSchema);
            //    }

            //    byte[] datasetBytes = ms.ToArray();
            //    datasetPartLength = datasetBytes.Length;

            //    dataBytes = new byte[8 + datasetPartLength];

            //    Buffer.BlockCopy(BitConverter.GetBytes(formatFlag), 0, dataBytes, 0, 8);
            //    Buffer.BlockCopy(datasetBytes, 0, dataBytes, 8, (int)datasetPartLength);
            //}
            
            return dataBytes;
        }

        public static byte[] ConvertToByteArray(this DataSet instance, List<string[]> binaryTableColumnInfos)
        {
            /* 1.FORMAT ข้อมูล ประเภท DatSet ที่มีแต่ข้อมูล text
             * [format flag (8)] : format flag สำหรับจำแนกข้อมูลว่ามีข้อมูลที่เป็น binary ร่วมอยู่ด้วยหรือไม่ (0 = ไม่มีข้อมูล binary, 1 = มีข้อมูล binary ร่วมด้วย)
             * [dataset data bytes] : ข้อมูล bytes ของ dataset
             * 
             * 2.FORMAT ข้อมูล ประเภท DatSet ที่มีข้อมูล binary เช่น รูปภาพ หรือเอกสารอื่นๆร่วมด้วย
             * [format flag (8)] : format flag สำหรับจำแนกข้อมูลว่ามีข้อมูลที่เป็น binary ร่วมอยู่ด้วยหรือไม่ (0 = ไม่มีข้อมูล binary, 1 = มีข้อมูล binary ร่วมด้วย)
             * [dataset bytes length (8)] : ความยาวของข้อมูลส่วนของ dataset (without binaries)
             * [binaries bytes length (8)] : ความยาวของข้อมูลส่วนที่เป็น binaries
             * [column info bytes length (8)] :  ความยาวของข้อมูลส่วนที่เป็น column info
             * [dataset data bytes] : ข้อมูล bytes ของ dataset 
             * [binaries data bytes] : ข้อมูล bytes ของ binary data เช่น  รูปภาพ, เอกสารต่างๆ
             * [column info data bytes] : ข้อมูล bytes ของข้อมูล column
             */

            if (instance == null)
                return null;

            byte[] dataBytes = null;

            Func<DataSet, bool> checkTblExists = (ds) =>
            {
                foreach(string[] tblInfo in binaryTableColumnInfos)
                {
                    if (ds.Tables.Contains(tblInfo[0]) && ds.Tables[tblInfo[0]].Rows.Count > 0)                        
                    {
                        return true;
                    }
                }
                return false;
            };

            if (!checkTblExists(instance))
            {
                dataBytes = instance.ConvertToByteArray();
            }
            else
            {

                Dictionary<string, List<byte[]>> binaryItems = new Dictionary<string, List<byte[]>>();
                string tableName = string.Empty;
                string binaryColumnName = string.Empty;
                DataTable table = null;
                List<byte[]> binList = null;

                foreach (string[] tblInfo in binaryTableColumnInfos)
                {
                    tableName = tblInfo[0];
                    binaryColumnName = tblInfo[1];

                    if (instance.Tables.Contains(tableName))
                    {
                        binList = new List<byte[]>();

                        table = instance.Tables[tableName];

                        table.Columns[binaryColumnName].ReadOnly = false;

                        foreach (DataRow row in table.Rows)
                        {
                            if (row[binaryColumnName] == DBNull.Value)
                            {
                                binList.Add(null);
                            }
                            else
                            {
                                binList.Add(row[binaryColumnName] as byte[]);
                                row[binaryColumnName] = DBNull.Value;
                            }
                        }

                        binaryItems.Add(tableName, binList);
                    }
                }

                

                byte[] datasetBytes = null;

                using (MemoryStream ms = new MemoryStream())
                {
                    instance.WriteXml(ms, XmlWriteMode.WriteSchema);
                    datasetBytes = ms.ToArray();
                }

                //using (MemoryStream ms = new MemoryStream())
                //{
                //    using (XmlTextWriter w = new XmlTextWriter(ms, Encoding.UTF8))
                //    {
                //        instance.WriteXml(w, XmlWriteMode.WriteSchema);
                //    }
                //    datasetBytes = ms.ToArray();
                //}


                byte[] binariesBytes = binaryItems.Serialize();
                byte[] colInfoBytes = binaryTableColumnInfos.Serialize();

                long formatFlag = 1;    //has binary data
                long datasetPartLength = (long)datasetBytes.Length;
                long binariesPartLength = (long)binariesBytes.Length;
                long colInfoPartLength = (long)colInfoBytes.Length;

                byte[] formatFlagBytes = BitConverter.GetBytes(formatFlag);
                byte[] datasetLengthBytes = BitConverter.GetBytes(datasetPartLength);
                byte[] binariesLengthBytes = BitConverter.GetBytes(binariesPartLength);
                byte[] colInfoLengthBytes = BitConverter.GetBytes(colInfoPartLength);

                //create output bytes
                dataBytes = new byte[8 + 8 + 8 + 8 + datasetPartLength + binariesPartLength + colInfoPartLength];

                Buffer.BlockCopy(formatFlagBytes, 0, dataBytes, 0, 8);
                Buffer.BlockCopy(datasetLengthBytes, 0, dataBytes, 8, 8);
                Buffer.BlockCopy(binariesLengthBytes, 0, dataBytes, 16, 8);
                Buffer.BlockCopy(colInfoLengthBytes, 0, dataBytes, 24, 8);
                Buffer.BlockCopy(datasetBytes, 0, dataBytes, 32, (int)datasetPartLength);
                Buffer.BlockCopy(binariesBytes, 0, dataBytes, (int)(32 + datasetPartLength), (int)binariesPartLength);
                Buffer.BlockCopy(colInfoBytes, 0, dataBytes, (int)(32 + datasetPartLength + binariesPartLength), (int)colInfoPartLength);

            }

            return dataBytes;
        }

        public static byte[] ConvertToByteArray(this DataSet instance, string tableName, string binaryColumnName)
        {
            List<string[]> binaryTableInfos = new List<string[]>();
            binaryTableInfos.Add(new string[] { tableName, binaryColumnName });

            return instance.ConvertToByteArray(binaryTableInfos);
            
        }

        public static DataSet ConvertToDataSet(this byte[] instance)
        {
            /* 1.FORMAT ข้อมูล ประเภท DatSet ที่มีแต่ข้อมูล text
             * [format flag (8)] : format flag สำหรับจำแนกข้อมูลว่ามีข้อมูลที่เป็น binary ร่วมอยู่ด้วยหรือไม่ (0 = ไม่มีข้อมูล binary, 1 = มีข้อมูล binary ร่วมด้วย)
             * [dataset data bytes] : ข้อมูล bytes ของ dataset
             * 
             * 2.FORMAT ข้อมูล ประเภท DatSet ที่มีข้อมูล binary เช่น รูปภาพ หรือเอกสารอื่นๆร่วมด้วย
             * [format flag (8)] : format flag สำหรับจำแนกข้อมูลว่ามีข้อมูลที่เป็น binary ร่วมอยู่ด้วยหรือไม่ (0 = ไม่มีข้อมูล binary, 1 = มีข้อมูล binary ร่วมด้วย)
             * [dataset bytes length (8)] : ความยาวของข้อมูลส่วนของ dataset (without binaries)
             * [binaries bytes length (8)] : ความยาวของข้อมูลส่วนที่เป็น binaries
             * [column info bytes length (8)] :  ความยาวของข้อมูลส่วนที่เป็น column info
             * [dataset data bytes] : ข้อมูล bytes ของ dataset 
             * [binaries data bytes] : ข้อมูล bytes ของ binary data เช่น  รูปภาพ, เอกสารต่างๆ
             * [column info data bytes] : ข้อมูล bytes ของข้อมูล column
             */


            if (instance == null)
                return null;

            //get format
            byte[] formatBytes = new byte[8];

            Buffer.BlockCopy(instance, 0, formatBytes, 0, 8);

            long formatFlag = BitConverter.ToInt64(formatBytes, 0);

            if (formatFlag == 0)
            {
                //dataset only, no binary data

                DataSet ds = new DataSet();
                ds.EnforceConstraints = false;

                using (MemoryStream ms = new MemoryStream(instance))
                {
                    ms.Seek(8, SeekOrigin.Begin);
                    ds.ReadXml(ms);
                }                

                return ds;
            }
            else if (formatFlag == 1)
            {
                //dataset contains binary data


                //get length of each part
                byte[] dataSetPartLengthBytes = new byte[8];
                byte[] binariesPartLengthBytes = new byte[8];
                byte[] colInfoPartLengthBytes = new byte[8];

                Buffer.BlockCopy(instance, 8, dataSetPartLengthBytes, 0, 8);
                Buffer.BlockCopy(instance, 16, binariesPartLengthBytes, 0, 8);
                Buffer.BlockCopy(instance, 24, colInfoPartLengthBytes, 0, 8);

                long dataSetPartLength = BitConverter.ToInt64(dataSetPartLengthBytes, 0);
                long binariesPartLength = BitConverter.ToInt64(binariesPartLengthBytes, 0);
                long colInfoPartLength = BitConverter.ToInt64(colInfoPartLengthBytes, 0);

                //get dataset
                DataSet ds = new DataSet();
                ds.EnforceConstraints = false;

                using (MemoryStream ms = new MemoryStream(instance, 32, (int)dataSetPartLength))
                {
                    ms.Seek(0, SeekOrigin.Begin);
                    ds.ReadXml(ms);
                }

                //get binary data
                byte[] binariesDataBytes = new byte[binariesPartLength];

                Buffer.BlockCopy(instance, (int)(32 + dataSetPartLength), binariesDataBytes, 0, (int)binariesPartLength);

                Dictionary<string,List<byte[]>> binaryItems = binariesDataBytes.DeSerialize<Dictionary<string,List<byte[]>>>();


                //get colInfo
                byte[] colInfoBytes = new byte[colInfoPartLength];

                Buffer.BlockCopy(instance, (int)(32 + dataSetPartLength + binariesPartLength), colInfoBytes, 0, (int)colInfoPartLength);

                List<string[]> tblInfos = colInfoBytes.DeSerialize<List<string[]>>();

                //set binary data back to dataset

                DataTable table = null;
                List<byte[]> binList = null;
                string tableName = string.Empty;
                string columnName = string.Empty;

                foreach (string[] tblInfo in tblInfos)
                {
                    tableName = tblInfo[0];
                    columnName = tblInfo[1];

                    if (ds.Tables.Contains(tableName))
                    {
                        table = ds.Tables[tableName];
                        binList = binaryItems[tableName];

                        int index = 0;
                        foreach (DataRow row in table.Rows)
                        {
                            byte[] data = binList[index];

                            if (data == null)
                            {
                                row[columnName] = DBNull.Value;
                            }
                            else
                            {
                                row[columnName] = data;
                            }

                            index++;
                        }
                    }
                }                

                return ds;
            }
            else
            {
                return null;
            }


        }

    }

    public static class DataTableExtensions
    {
        public static void LoadFromReader(this DataTable instance, DbDataReader reader)
        {
            int fieldsCount = 0;
            
            //create columns
            fieldsCount = reader.FieldCount;

            for (int i = 0; i < fieldsCount; i++)
            {
                instance.Columns.Add(new DataColumn(reader.GetName(i), reader.GetFieldType(i)));
            }      

            while (reader.Read())
            {                

                DataRow row = instance.NewRow();

                for (int i = 0; i < fieldsCount; i++)
                {
                    row[reader.GetName(i)] = reader.GetValue(i);
                }

                instance.Rows.Add(row);
            }

            reader.Close();
        }
    }

    public static class DateTimeExtensions
    {
        private static CultureInfo thCulture = new CultureInfo("th-TH");
        private static CultureInfo enCulture = new CultureInfo("en-US");

        public static string ToThString(this DateTime instance, string format)
        {
            return instance.ToString(format, thCulture);
        }

        public static string ToEnString(this DateTime instance, string format)
        {
            return instance.ToString(format, enCulture);
        }

        public static string ToThString(this DateTime? instance, string format)
        {
            if (instance.HasValue)
                return instance.Value.ToString(format, thCulture);
            else
                return "";
        }

        public static string ToEnString(this DateTime? instance, string format)
        {
            if (instance.HasValue)
                return instance.Value.ToString(format, enCulture);
            else
                return "";
        }

        public static DateTime StartOfDay(this DateTime instance)
        {
            return new DateTime(instance.Year, instance.Month, instance.Day, 0, 0, 0);
        }

        public static DateTime EndOfDay(this DateTime instance)
        {
            return new DateTime(instance.Year, instance.Month, instance.Day, 23, 59, 59);
        }
    }

    public static class ExceptionExtensions
    {
        public static string GetShortDescription(this Exception instance)
        {
            return string.Format(
                @"{0} : {1}", instance.GetType().ToString(), instance.Message);

        }
    }

    public static class LINQToXmlExtensions
    {
        public static string GetStringValue(this XAttribute instance, string valueIfNotExist)
        {
            if (instance == null)
                return valueIfNotExist;
            else
                return instance.Value;
        }

        public static int GetIntValue(this XAttribute instance, int valueIfNotExist)
        {
            if (instance == null)
                return valueIfNotExist;
            else
                return Convert.ToInt32(instance.Value);
        }
    }

    public static class StringExtensions
    {
        public static string AutoTrim(this string instance)
        {
            if (instance == null)
                return null;
            else
                return instance.Trim();
            
        }

        public static string ToSafeSearchString(this string instance)
        {
            if (instance == null)
                return null;
            else
                return instance.Replace("'", "''");
        }


        public static string ToBase64HashString(this string instance, HashAlgorithm algo)
        {
            string hashOutput = string.Empty;

            if (algo == HashAlgorithm.SHA256)
            {
                var sha = System.Security.Cryptography.SHA256Managed.Create();

                byte[] hashBytes = sha.ComputeHash(Encoding.UTF8.GetBytes(instance));

                hashOutput = Convert.ToBase64String(hashBytes);
            }

            return hashOutput;
        }

        public static bool IsNullOrBlank(this string instance)
        {
            return (instance == null || instance.Trim().Length == 0);
        }

        public static bool IsNull(this string instance)
        {
            return (instance == null);
        }
        public static int ToInt(this string instance)
        {
            try
            {
                return Convert.ToInt32(instance);
            }
            catch
            {
                return 0;
            }
        }

        public static bool IsNull(this decimal? instance)
        {
            return (instance == null);
        }

        public static bool IsNumericDecimal(this char instance)
        {
            ////เดิมเป็นแบบนี้ 20100404
            //return !char.IsDigit(instance) && instance != '\b' && instance != '.' && instance != ',';

            //ใหม่ 20100405
            return !char.IsDigit(instance) && instance != '\b' && instance != '.';
        }

        public static bool IsNumericInt(this char instance)
        {
            return !char.IsDigit(instance) && instance != '\b';
        }

        public static bool IsNullOrNoRows(this DataTable dt)
        {
            bool result = true;

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    result = false;
                }
            }
            return result;
        }

        public static string ToYandNFlag(this bool b)
        {
            if (b)
            {
                return "Y";
            }
            else
            {
                return "N";
            }
        }

        public static string ToNandYFlag(this bool b)
        {
            if (b)
            {
                return "N";
            }
            else
            {
                return "Y";
            }
        }

        public static string ToYandNull(this bool b)
        {
            if (b)
            {
                return "Y";
            }
            else
            {
                return null;
            }
        }

        public static string To0Or1Flag(this bool b)
        {
            if (b)
            {
                return "1";
            }
            else
            {
                return "0";
            }
        }

        private static char[] comma = new char[] { ',' };
        public static string[] SplitByComma(this string instance)
        {
            var items = instance.Split(comma, StringSplitOptions.RemoveEmptyEntries);

            int len = items.Length;
            for(int i = 0;i<len;i++)
            {
                items[i] = items[i].Trim();
            }

            return items;
        }

    }


    public enum HashAlgorithm
    {
        SHA256
    }

}
