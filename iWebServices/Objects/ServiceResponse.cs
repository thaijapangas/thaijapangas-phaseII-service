﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.Data;

namespace iWebServices.Objects
{
    public class ServiceResponse
    {
        public ServiceResponse()
        {

        }

        [MessageHeader()]
        public string Id { get; set; }

        [MessageBodyMember(Order = 1)]
        public byte[] Data { get; set; }

        [MessageBodyMember(Order = 2)]
        public string Code { get; set; }

        [MessageBodyMember(Order = 3)]
        public string Detail { get; set; }

        [MessageBodyMember(Order = 4)]
        public DataSet ds { get; set; }
    }
}