﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data;
using iEntitiesTmp.DistributionRet;
using iWebServices.Objects;

namespace iWebServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService
    {
        #region Start

        [OperationContract]
        string GetData(int value);
        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        #endregion

        // TODO: Add your service operations here

        #region Test ConnectionDB

        /*
        [OperationContract]
        string CheckExpireSoftware();
        */
        [OperationContract]
        bool ConnectDB();
        [OperationContract]
        object SqlConn();
        /*
        [OperationContract]
        string ValidationLicence(string _licence_keys);
        */

        #endregion

        #region GetDate

        [OperationContract]
        object GetDateServer();

        #endregion

        #region IsOnline

        [OperationContract]
        string IsOnline();

        #endregion

        #region Distribution Ret

        [OperationContract]
        string InsUpdDistributionRet(byte[] Data);
        [OperationContract]
        string DoInsUpdDistributionRet(string serial_number, string cust_id, string doc_no, string vehicle, string empty_or_full, string create_date);
        
        #endregion

        #region GI Delivery

        [OperationContract]
        string InsUpdGIDelivery(byte[] Data);
        [OperationContract]
        string DoInsUpdGIDelivery(string serial_number, string do_no, string create_date);

        #endregion

        #region GI Ref IO Empty

        [OperationContract]
        string InsUpdGIRefIOEmpty(byte[] Data);
        [OperationContract]
        string DoInsGIRefIOEmpty(string serial_number, string do_no, string create_date);

        #endregion

        #region GR Production

        [OperationContract]
        string InsUpdGRProduction(byte[] Data);
        [OperationContract]
        string DoInsGRProduction(string serial_number, string pre_order, string batch, string create_date);

        #endregion

        #region Download Customer

        [OperationContract]
        DataTable DoLoadCustomer();
        [OperationContract]
        ServiceResponse DoLoadCustomerByte();
        [OperationContract]
        DataTable DoLoadCustomerStored();

        #endregion
    }

    #region DataContract Start

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }

    #endregion
}