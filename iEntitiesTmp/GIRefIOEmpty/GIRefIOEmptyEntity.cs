﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Text;

namespace iEntitiesTmp.GIRefIOEmpty
{
    [DataContract]
    public class GIRefIOEmptyEntity
    {
        [DataMember]
        public string serial_number { get; set; }
        [DataMember]
        public string doc_no { get; set; }
        [DataMember]
        public string create_by { get; set; }
        [DataMember]
        public DateTime? create_date { get; set; }
        [DataMember]
        public string update_by { get; set; }
        [DataMember]
        public DateTime? update_date { get; set; }
    }
}