﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Text;

namespace iEntitiesTmp.GRProduction
{
    [DataContract]
    public class GRProductionEntity
    {
        [DataMember]
        public string serial_number { get; set; }
        [DataMember]
        public string pre_order { get; set; }
        [DataMember]
        public string batch { get; set; }
        [DataMember]
        public string create_by { get; set; }
        [DataMember]
        public DateTime? create_date { get; set; }
        [DataMember]
        public string update_by { get; set; }
        [DataMember]
        public DateTime? update_date { get; set; }
    }
}